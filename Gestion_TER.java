package fr.umfds.TP1Gestion_Projet_maven;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Gestion_TER  {
	//cette classe va nous permettre daffecter les sujetau groupe donc en utillisant la methode hongroise
	
	private ArrayList<Sujet> ListeSujet;
	private ArrayList<Voeux> ListeVoeux;
	private ArrayList<Groupe> ListeGroupe;
	
	
	//CONSTRUCTEUR
	public Gestion_TER(ArrayList<Sujet> ls, ArrayList<Voeux> lv, ArrayList<Groupe> lg) {
		this.ListeSujet = ls;
		this.ListeVoeux = lv;
		this.ListeGroupe = lg;
	}
	
	
	//Acceseur pour recuperer le sujet en taille i
	
	
	public void addGroupe(String nom) {
		 this.ListeGroupe.add(new Groupe(nom));
	}
	public void addSujet(String titre) {
		 this.ListeSujet.add(new Sujet(titre));
	}

		  
		  
	//Sujet
	public Sujet getSujet(int i) {
		for (int j=0; j<ListeSujet.size(); j++) {
			if (j==i) {
				return ListeSujet.get(j);
			}
		}
		return null;
	}
	//voeux
	public Voeux getVoeux(int i) {
		for (int j=0; j<ListeVoeux.size(); j++) {
			if (j==i) {
				return ListeVoeux.get(j);
			}
		}
		return null;
	}
	//Groupe
	public Groupe getGroupe(int i) {
		for (int j=0; j<ListeGroupe.size(); j++) {
			if (j==i) {
				return ListeGroupe.get(j);
			}
		}
		return null;
	}
	
	
	//SERIALISER
	
	//GROUPE
	//il y a seulement un groupe dans la liste donc on peut direct mettre ListeGroupe[0]
	public static void serialiserGp(Groupe gp) throws StreamWriteException, DatabindException, IOException{   //pour serialiser un seul groupe
		
		ObjectMapper objectMapperGp = new ObjectMapper();

	    File fichierGp = new File ("target/groupe.json");
	    objectMapperGp.writeValue(fichierGp, gp);
	}
	
	public static void serialiserGp_liste(ArrayList<Groupe> Gp) throws StreamWriteException, DatabindException, IOException{   //pour serialiser une liste de groupe
		
		ObjectMapper objectMapperGp = new ObjectMapper();

	    File fichierGp = new File ("target/groupe.json");
	    objectMapperGp.writeValue(fichierGp, Gp);
	}
	
	//SUJET
	public static void serialiserSj(Sujet sj) throws StreamWriteException, DatabindException, IOException {   //pour serialiser un seul groupe
		
		ObjectMapper objectMapperSj = new ObjectMapper();

	    File fichierSj = new File ("target/sujet.json");
	    objectMapperSj.writeValue(fichierSj, sj);
	}
	
	public static void serialiserSj_liste(ArrayList<Sujet> Sj) throws StreamWriteException, DatabindException, IOException  {   //pour serialiser une liste de groupe
		
		ObjectMapper objectMapperSj = new ObjectMapper();

	    File fichierSj = new File ("target/sujet.json");
	    objectMapperSj.writeValue(fichierSj, Sj);
	}
	
	
	//VOEUX
	public static void serialiserVx(Voeux vx) throws StreamWriteException, DatabindException, IOException{   //pour serialiser un seul groupe
		
		ObjectMapper objectMapperVx = new ObjectMapper();

	    File fichierVx = new File ("target/voeux.json");
	    objectMapperVx.writeValue(fichierVx, vx);
	}
	
	public static void serialiserVx_liste(ArrayList<Voeux> Vx) throws StreamWriteException, DatabindException, IOException{   //pour serialiser une liste de groupe
		
		ObjectMapper objectMapperVx = new ObjectMapper();

	    File fichierVx = new File ("target/voeux.json");
	    objectMapperVx.writeValue(fichierVx, Vx);
	}
    

	
	//DESERIALISER
	
	//GROUPE
	public static List<Groupe> deserialiserGp(File f) throws StreamWriteException, DatabindException, IOException{   //pour serialiser une liste de groupe
			
		ObjectMapper objectMapperGp = new ObjectMapper();
		
		File fichierGp = f;
	    List<Groupe> liste_gpDes = Arrays.asList(objectMapperGp.readValue(fichierGp, Groupe[].class));
	    return liste_gpDes;
	}
	
	
	//SUJET
	public static List<Sujet> deserialiserSj(File f) throws StreamWriteException, DatabindException, IOException {   //pour serialiser un seul groupe
			
		ObjectMapper objectMapperSj = new ObjectMapper();
		
		File fichierSj = f;
	    List<Sujet> liste_sjDes = Arrays.asList(objectMapperSj.readValue(fichierSj, Sujet[].class));
	    return liste_sjDes;
	}
	
		
	//VOEUX
	public static List<Voeux> deserialiserVx(File f) throws StreamWriteException, DatabindException, IOException{   //pour serialiser un seul groupe
			
		ObjectMapper objectMapperVx = new ObjectMapper();
		
		File fichierVx = f;
		//comme on a une liste il faut donc ouvrir la liste
		
	    List<Voeux> liste_vxDes = Arrays.asList(objectMapperVx.readValue(fichierVx, Voeux[].class));
	    return liste_vxDes;
	}


}
