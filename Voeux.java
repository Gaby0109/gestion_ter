package fr.umfds.TP1Gestion_Projet_maven;


import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

class Voeux {
  private Groupe gp;
  private ArrayList<Sujet> listeSujet;

  @JsonCreator
  public Voeux(@JsonProperty("gp") Groupe gp, @JsonProperty("listeSujet") ArrayList<Sujet> listeSujet){
    this.gp=gp;
    this.listeSujet=listeSujet;
  }
 
  public Groupe getGp(){
    return this.gp;
  }
  
  //ici on retourne la liste des sujet donc le voeux entier
  public ArrayList<Sujet> getListeSujet(){
	return this.listeSujet;
  }
  
  //dans cette methode on veut le voeux i
  public Sujet getVoeuxI(int i) {
	  for (int j=0; j<5; j++) {
		  if (j==i-1){
			  return listeSujet.get(j);
		  }
	  }
	  return null;
  }
 
	  
}